//
//  MainViewController.m
//  KillTheHipsters
//
//  Created by Diogo Costa on 03/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import "HipsterView.h"
#import "PlayerData.h"
#import "Timer.h"
#import "Constants.h"
#import "JSONKit.h"
#import "AlertView.h"

#define HIPSTERCOUNT 20

@interface MainViewController()

- (void)startGame;

@end

@implementation MainViewController

@synthesize pointsLabel;
@synthesize timeLabel;

- (void)viewDidLoad {
	[[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(changePlayerScore) 
                                                 name:PLAYER_SCORED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(changeCountdownTimer) 
                                                 name:COUNTDOWN 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(finishGame) 
                                                 name:GAME_FINISHED 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(restartGame) 
                                                 name:RESTART_GAME 
                                               object:nil];
	interval = 1.0;

	fatorDif = 12;
	
	int xLimit = self.view.frame.size.height - 64 - 10;
	NSLog(@"xlimit %i",xLimit);

	int yLimit = self.view.frame.size.width - 64 - 10;
	NSLog(@"ylimit %i",yLimit);
	srand(time(NULL));

	[super viewDidLoad];
	hipsterArray = [[NSMutableArray alloc] init];
	for (int i =0; i<HIPSTERCOUNT; i++) {
		int xValue = rand() % xLimit + 10;
		NSLog(@"xValue %i",xValue);
		
		int yValue = rand() % yLimit + 10;
		NSLog(@"yValue %i",yValue);
		
		HipsterView *hipsterView = [[[HipsterView alloc] initWithFrame:CGRectMake(xValue, yValue, 64, 64)] autorelease];
		hipsterView.image = [UIImage imageNamed:@"hipsterIcon.png"];
		hipsterView.alpha = 0.0;
		[hipsterArray addObject:hipsterView];
		[self.view addSubview:hipsterView];
	}
	
	srand(time(NULL));
}

- (void)viewWillAppear:(BOOL)animated{
    [self startGame];
}

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
	[self dismissModalViewControllerAnimated:YES];
}


- (IBAction)showInfo:(id)sender {    
	FlipsideViewController *controller = [[[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil] autorelease];
	controller.delegate = self;
	if ([timer isValid]) [timer invalidate];

	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

- (void)dealloc {
    [super dealloc];
}


/* GAME LOOP */
- (void)showHipster{
	
	if ([PlayerData sharedPlayerData].hitTargets == fatorDif) {
		interval /= 1.3;
		fatorDif = fatorDif + fatorDif*2;
		NSLog(@"intervalo: %f",interval);
		NSLog(@"fator: %i",fatorDif);
        
        //		[PlayerData sharedPlayerData].hitTargets = 0;
		timer = [NSTimer scheduledTimerWithTimeInterval:interval 
                                                 target:self 
                                               selector:@selector(showHipster) 
                                               userInfo:nil 
                                                repeats:YES];
	}
	
	int index = rand() % HIPSTERCOUNT;
	HipsterView *hipster = [hipsterArray objectAtIndex:index];
	hipster.alpha = 1.0;
}

- (void)finishGame {
    if ([timer isValid]) [timer invalidate];
    
    AlertView *alertView = [[AlertView alloc] initWithTitle:@"Kill The Hipster" 
                                                    message:@"Rank this? The game will restart." 
                                                    delegate:nil 
                                            cancelButtonTitle:@"No" 
                                            otherButtonTitles:@"Yes", nil];
    [alertView setDelegate:alertView];
    [alertView show];
    [alertView release];
}

- (void)restartGame {
    [[Timer sharedTimer] restartCountdownTimer];
    [[PlayerData sharedPlayerData] restartPlayerData];
    for (HipsterView *hipster in hipsterArray) 
        hipster.alpha = 0;
    interval = 1.0;
	fatorDif = 12;
    [self startGame];
}

- (void)startGame {
    [[Timer sharedTimer] startCountdownTimer];
    timer = [NSTimer scheduledTimerWithTimeInterval:interval 
                                             target:self 
                                           selector:@selector(showHipster) 
                                           userInfo:nil 
                                            repeats:YES];
}

/* HUD UPDATE */
- (void)changePlayerScore {
    self.pointsLabel.text = [NSString stringWithFormat:@"%i", [PlayerData sharedPlayerData].amountPoints];
}

- (void)changeCountdownTimer {
    self.timeLabel.text = [Timer sharedTimer].humanTime;
}

@end
