//
//  Contants.h
//  KillTheHipsters
//
//  Created by Filipe Pereira on 18/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

// Notifications
#define COUNTDOWN           @"COUNTDOWN"
#define PLAYER_SCORED       @"PLAYER_SCORED"
#define GAME_FINISHED       @"GAME_FINISHED"
#define RESTART_GAME        @"RESTART_GAME" 

// Other constants
#define TIME_COUNTER        10