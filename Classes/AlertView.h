//
//  AlertView.h
//  KillTheHipsters
//
//  Created by Filipe Pereira on 18/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertView : UIAlertView <UIAlertViewDelegate>

@end
