//
//  Timer.h
//  KillTheHipsters
//
//  Created by Filipe Pereira on 18/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Timer : NSObject {
    int counter;
    NSString *humanTime;
}

@property (nonatomic, assign) NSString *humanTime;

+ (Timer *)sharedTimer;
- (void)startCountdownTimer;
- (void)restartCountdownTimer;

@end
