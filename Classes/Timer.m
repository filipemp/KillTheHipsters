//
//  Timer.m
//  KillTheHipsters
//
//  Created by Filipe Pereira on 18/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Timer.h"
#import "Constants.h"

@interface Timer()

+ (NSString *)convertToHumanTime:(int)counter;

@end

@implementation Timer

@synthesize humanTime;
static Timer *sharedTimer = nil;

+ (Timer *)sharedTimer {
    if (!sharedTimer)
        sharedTimer = [[Timer alloc] init];
    return sharedTimer;
}

- (id)init {
    if ((self=[super init])) {
        counter = 0;
        humanTime = nil;
    }
	
	return self;
}

- (void)startCountdownTimer {
    counter = TIME_COUNTER;
    humanTime = [Timer convertToHumanTime:counter];
    [[NSNotificationCenter defaultCenter] postNotificationName:COUNTDOWN object:nil];
    
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(advanceTimer:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)advanceTimer:(NSTimer *)timer {
    counter--;
    humanTime = [Timer convertToHumanTime:counter];
    [[NSNotificationCenter defaultCenter] postNotificationName:COUNTDOWN object:nil];
    if (counter <= 0) { 
        [timer invalidate];
        [[NSNotificationCenter defaultCenter] postNotificationName:GAME_FINISHED object:nil];
    }
}

- (void)restartCountdownTimer {
    counter = TIME_COUNTER;
}

+ (NSString *)convertToHumanTime:(int) counter {
    int min = counter/60;
    int sec = counter - (min * 60);
    NSString *retVal;
    if (sec < 10)
        retVal = [NSString stringWithFormat:@"%i:0%i", min, sec];
    else
        retVal = [NSString stringWithFormat:@"%i:%i", min, sec];
         
    return retVal;
}

@end
